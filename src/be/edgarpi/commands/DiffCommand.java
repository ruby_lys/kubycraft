package be.edgarpi.commands;

import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DiffCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {

		if (sender instanceof Player) {
			Player p = (Player) sender;
			World world = p.getWorld();
				if (cmd.getName().equalsIgnoreCase("diff")) {

					if (args.length == 0) {
						p.sendMessage(ChatColor.BLUE + world.getDifficulty().name());

					}

					if (args.length == 1) {

						if (args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("peaceful")) {
							world.setDifficulty(Difficulty.PEACEFUL);
							p.sendMessage("Change to peaceful");

						}
						if (args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("easy")) {
							world.setDifficulty(Difficulty.EASY);
							p.sendMessage("Change to easy");

						}

						if (args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("normal")) {
							world.setDifficulty(Difficulty.NORMAL);
							p.sendMessage("Change to normal");
						}
						if (args[0].equalsIgnoreCase("3") || args[0].equalsIgnoreCase("hard")) {
							world.setDifficulty(Difficulty.HARD);
							p.sendMessage("Change to hard");
						}
					}

				}
				return true;
		}
		return false;
	}

}
