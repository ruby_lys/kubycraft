package be.edgarpi.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class VanishCommand implements CommandExecutor, Listener {
	ArrayList<String> trigger = new ArrayList<String>();
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {
		Player p = (Player)sender;
		if (args.length > 0) {
			p.sendMessage("�cTu as mis trop d'arguments.");
		}
			if (sender instanceof Player) {
				for (Player all : Bukkit.getOnlinePlayers()) {
					if (trigger.contains(p.getUniqueId().toString())) {
						all.showPlayer(p);
						p.showPlayer(p);
						p.removePotionEffect(PotionEffectType.INVISIBILITY);
						p.sendMessage("�aVotre couverture a cass�e");
						trigger.remove(p.getUniqueId().toString());
					}else if(!trigger.contains(p.getUniqueId().toString())) {
						all.hidePlayer(p);
						p.hidePlayer(p);
						p.sendMessage("�aVous �tes d�sormais cacher de tous");
						p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, true), true);
						trigger.add(p.getUniqueId().toString());
					}
					
				}
			}
		
		return true;
	}
	
	public void onJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		for (Player all : Bukkit.getOnlinePlayers()) {
			if (trigger.contains(p.getUniqueId().toString())) {
				all.showPlayer(p);
				p.removePotionEffect(PotionEffectType.INVISIBILITY);
				
				trigger.remove(p.getUniqueId().toString());
			}else if(!trigger.contains(p.getUniqueId().toString())) {
				all.hidePlayer(p);
				p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, true), true);
				trigger.add(p.getUniqueId().toString());
			}
		}
	}

}
