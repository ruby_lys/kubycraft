package be.edgarpi.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MultiCommand implements CommandExecutor {

	@SuppressWarnings("unused")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {
		ArrayList<String> toggle = new ArrayList<String>();
		Player p = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("broadcast")) {
			if (args.length == 0) {
				return false;
			}
			if (args.length == 1) {
				msg = args[0];
				Bukkit.getServer().broadcastMessage(ChatColor.AQUA + "[BROADCAST]>" + ChatColor.DARK_BLUE
						+ sender.getName() + ">" + ChatColor.YELLOW + msg);
				p.sendMessage(ChatColor.GREEN + "Tu as envoyer ton Broadcast !");
				return true;
			}
		}
		return false;
	}

}
