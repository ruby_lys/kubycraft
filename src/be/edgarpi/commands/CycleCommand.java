package be.edgarpi.commands;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CycleCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {
		Player p = (Player) sender;
		World world = p.getWorld();
			if (cmd.getName().equalsIgnoreCase("day")) {
				world.setTime(0);
				p.sendMessage("Set time to day");
				return true;
			}
			if (cmd.getName().equalsIgnoreCase("night")) {
				world.setTime(13000);
				p.sendMessage("Set time to night");
				return true;
			}
			if (cmd.getName().equalsIgnoreCase("sun")) {
				world.setStorm(false);
				p.sendMessage("Set weather to sun");
				return true;
			}
			if (cmd.getName().equalsIgnoreCase("storm")) {
				world.setStorm(true);
				p.sendMessage("Set weather to storm");
				return true;
			}
		return false;
	}

}
