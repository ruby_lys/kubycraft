package be.edgarpi.commands;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import be.edgarpi.utils.Messages;

public class GMCommand implements CommandExecutor {
	Messages messages = new Messages();
	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {

		if (sender instanceof Player) {
			Player p = (Player) sender;
				if (cmd.getName().equalsIgnoreCase("gm")) {

					if (args.length == 0) {
						return false;

					}
					if (args.length == 1) {

						if (args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("survival")) {

							p.setGameMode(GameMode.SURVIVAL);

						}
						if (args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("creative")) {

							p.setGameMode(GameMode.CREATIVE);

						}

						if (args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("adventure")) {
							p.setGameMode(GameMode.ADVENTURE);
						}
					}
					return true;
				}
			}
		return false;
	}

}
