package be.edgarpi.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import be.edgarpi.utils.Messages;

public class InvCommand implements CommandExecutor {
	Messages messages = new Messages();
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (p.isOp()) {

				if (cmd.getName().equalsIgnoreCase("ec")) {

					if (args.length == 0) {
						p.openInventory(p.getEnderChest());
						return true;
					}
					if (args.length == 1) {
						Player target = Bukkit.getPlayer(args[0]);
						if (target == null) {
							messages.playerNotFound(p);
							return true;
						}
						p.openInventory(target.getEnderChest());
						return true;
					}
				}
				if (cmd.getName().equalsIgnoreCase("invsee")) {
					if (args.length == 0) {
						messages.syntaxeError("/invsee <player>", p);
						return true;
					}

					if (args.length == 1) {
						Player target = Bukkit.getPlayer(args[0]);
						if (target == null) {
							messages.playerNotFound(p);
							return true;
						}
						p.openInventory(target.getInventory());

						return true;
					}
				}
			}else {
				p.sendMessage(ChatColor.RED + "You not have permissions for use this command");
			}
		}
		return false;
	}

}
