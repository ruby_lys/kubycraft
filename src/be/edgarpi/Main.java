package be.edgarpi;


import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import be.edgarpi.commands.CycleCommand;
import be.edgarpi.commands.DiffCommand;
import be.edgarpi.commands.GMCommand;
import be.edgarpi.commands.InvCommand;
import be.edgarpi.commands.MultiCommand;
import be.edgarpi.commands.VanishCommand;
import be.edgarpi.listener.PlayerListener;

public class Main extends JavaPlugin {
	@Override
	public void onEnable() {
		PluginManager pm = Bukkit.getPluginManager();
		getLogger().info("KubyCraft Plugin as enable");
		getCommand("gm").setExecutor(new GMCommand());
		getCommand("diff").setExecutor(new DiffCommand());
		getCommand("ec").setExecutor(new InvCommand());
		getCommand("invsee").setExecutor(new InvCommand());
		getCommand("day").setExecutor(new CycleCommand());
		getCommand("night").setExecutor(new CycleCommand());
		getCommand("sun").setExecutor(new CycleCommand());
		getCommand("storm").setExecutor(new CycleCommand());
		getCommand("broadcast").setExecutor(new MultiCommand());
		getCommand("vanish").setExecutor(new VanishCommand());
		
		
		pm.registerEvents(new VanishCommand(), this);
		pm.registerEvents(new PlayerListener(this), this);
		
		getConfig().options().copyDefaults(true);
		saveConfig();
		
	}
	@Override
	public void onDisable() {
		getLogger().info("KubyCraft Plugin as disable");
	}
	
}
