package be.edgarpi.listener;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import be.edgarpi.Main;


public class PlayerListener implements Listener {
	private FileConfiguration config;

	public PlayerListener(Main main) {
		config = main.getConfig();

	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		String motd = config.getString("motd-join");
		p.sendMessage(ChatColor.GREEN + motd);
	}

}
